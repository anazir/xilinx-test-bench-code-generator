const input = document.querySelector('input[name="xilinx-inputs"]');
const limitInput = document.querySelector('input[name="limit"]');
const submit = document.querySelector('button');
const copyBtn = document.querySelector('#copy-btn');
const resultContainer = document.querySelector('#results');
let results = '';

const isValidInput = () => {
  // Check xilinx inputs
  const arr = input.value.split(' ').filter(a => a.length > 0);
  if(arr.length === 0) {
    return false;
  }

  const isValid = arr.reduce((pr, cur) => {
    const illegalCharacters = /[^\w(:)_]/g;

    if(cur.match(illegalCharacters)) {
      return false;
    } else {
      const busChars = /[(:)]/g;
      const correctBusFormat = /[\w_]+\(\d+:\d+\)/g;

      if(cur.match(busChars) && !cur.match(correctBusFormat)) {
        return false;
      }
      
      return pr;
    }
  }, true);

  // Check limit
  const limitInt = parseInt(limitInput.value || -1);
  // First make sure it is not a float
  limitInput.value = Math.trunc(limitInt);
  // Then check that it's no smaller that -1
  limitInput.value = (limitInt < -1) ? -1 : limitInt;
  
  return isValid;
};

const displayError = () => {
  resultContainer.innerHTML = '<div class="error">Wrong input.</div>';
};

const getInputs = (inputTxt) => {
  const inputArr = inputTxt
    .split(' ')
    .filter(a => a.length > 0)
    .map(a => {
      const openParenInd = a.indexOf('(');
      const colonInd = a.indexOf(':');

      if(openParenInd != -1) {
        const upperNum = parseInt(a.slice(openParenInd + 1, colonInd));
        const lowerNum = parseInt(a.slice(colonInd + 1, a.indexOf(')')));
        const busLength = upperNum - lowerNum + 1;

        return {
          type: 'bus',
          length: busLength,
          name: a.slice(0, openParenInd)
        };
      } else {
        return {
          type: 'input',
          name: a
        };
      }
    });

  const checkedRadio = document.querySelector('input[name="limit-method"]:checked');
  return {
    xilinxInputs: inputArr,
    limit: parseInt(limitInput.value),
    limitMethod: checkedRadio.value
  };
}
 
const decToNumBitBin = (decNum, bitNum) => {
  let binNum = decNum.toString(2);

  for(let i = binNum.length; i <= bitNum; i ++) {
    binNum = '0' + binNum;
  }

  return binNum;
}

const generateCode = (inputs) => {
  let code = '';

  const numOfBits = inputs.xilinxInputs.reduce((pr, cur) => {
    if(cur.type === 'bus') {
      return pr + cur.length;
    } else {
      return ++ pr;
    }
  }, 0);

  let linesOfCode = Math.pow(2, numOfBits);
  let lineOffset = 1;

  if(inputs.limit !== -1) {
    if(Math.trunc(linesOfCode / inputs.limit < 2)) {
      inputs.limitMethod = "first";
      console.log('h');
    }

    if(inputs.limitMethod === "spread-out") { 
      lineOffset = Math.trunc(linesOfCode / inputs.limit) || 1;
    }

    linesOfCode = (linesOfCode < inputs.limit) ? linesOfCode : inputs.limit;
  }

  for(let i = 0; i / lineOffset < linesOfCode; i += lineOffset) {
    const curLineNumInBin = decToNumBitBin(i, numOfBits);
    let bitsUsed = 0;

    const curLine = inputs.xilinxInputs.map(a => {
      const isBus = a.type === 'bus';
      const length = (isBus) ? a.length : 1;
      const typeOfQuote = (isBus) ? `"` : `'`;

      const bits = curLineNumInBin.slice(bitsUsed + 1, bitsUsed + length + 1);
      bitsUsed += length;

      return `${a.name}<=${typeOfQuote}${bits}${typeOfQuote}; `;
    }).join('') + ' Wait for 10 ms; <br>';

    code += curLine;
  }

  return code;
}

const displayCode = (code) => {
  resultContainer.innerHTML = code;
};

const runProgram = () => {
  if(!isValidInput()) {
    displayError();
  } else {
    const inputs = getInputs(input.value);
    const code = generateCode(inputs);
    displayCode(code);
    results = code;
  }
}

submit.addEventListener('click', runProgram);

const copyResults = async () => {
  runProgram();
  let data = results.replace(/<br>/gi, '\n');
  
  try {
    await navigator.clipboard.writeText(data);
  } catch (err) {
    alert('Can\'t copy');
  }
}

copyBtn.addEventListener('click', copyResults);

// Don't show copy button if the functionality isn't supported
if(!navigator.clipboard) {
  copyBtn.style.display = 'none';
}
