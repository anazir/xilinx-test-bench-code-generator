# Xilinx Test Bench Code Generator
### A tool that generates code for testing your schematics based on the inputs.

Just enter the names of your inputs in the text field separated by spaces.
If an input is a bus then write it in the format of BusName(num:num). ex: A(31:0)

